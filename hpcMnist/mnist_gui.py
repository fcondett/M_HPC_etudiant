import cv2 as cv
import numpy as np
import tensorflow.keras as kr

WIDTH, HEIGHT = 280, 280
WINDOW_NAME = "mnist_gui"
BRUSH_RADIUS = 20
BRUSH_COLOR = (255, 255, 255)
BACKGROUND_COLOR = 0

model = kr.models.load_model('mnist_model.h5')


def predict(img):
    ml_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    ml_img = cv.resize(ml_img, (28, 28))
    ml_img = ml_img.astype('float32')
    ml_img /= 255
    ml_img = ml_img.reshape(1, 28, 28, 1)
    prediction = model.predict_classes(ml_img)
    print(prediction[0])


def draw_line(event,x,y,flags,param):
    global ix,iy,drawing

    if event == cv.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y

    elif event == cv.EVENT_MOUSEMOVE:
        if drawing == True:
            cv.line(image,(x,y),(x,y),BRUSH_COLOR,BRUSH_RADIUS)

    elif event == cv.EVENT_LBUTTONUP:
        drawing = False
        #cv.line(image,(ix,iy),(x,y),BRUSH_COLOR,BRUSH_RADIUS)
        predict(image)

    #elif event == cv.EVENT_RBUTTONDOWN:
     #  image = np.zeros((512,512,3), np.uint8)

ix, iy = -1, -1
drawing = False
image = np.zeros((512,512,3), np.uint8)
cv.namedWindow(WINDOW_NAME)
cv.setMouseCallback(WINDOW_NAME,draw_line)

print("Draw a digit in the window:")
print(" -left click -> draw")
print(" - right click -> clear image")
print(" - esc -> quit")
while(1):
    cv.imshow(WINDOW_NAME,image)
    k = cv.waitKey(1) & 0xFF
    if k == 27:
        break

