#!/usr/bin/env python3

import cv2 as cv
import time
import sys

if __name__ == '__main__':

    # arguments
    if len(sys.argv) != 2:
        print("usage:", sys.argv[0], "<filename>")
        sys.exit(-1)
    FILENAME = sys.argv[1]

    # load input image
    imgInput = cv.imread(FILENAME)
    img_gray = cv.cvtColor(imgInput, cv.COLOR_BGR2GRAY)
    def update_win(x):
        (ret, img) = cv.threshold(img_gray,x,255,cv.THRESH_BINARY)
        cv.imshow('kermorvan', img)

    # affiche l'image + trackbar
    cv.namedWindow('kermorvan')
    cv.createTrackbar('threshold', 'kermorvan', 0, 255, update_win)
    cv.setTrackbarPos('threshold', 'kermorvan', 25)

    while True:
        if cv.waitKey() == 27:
            break

