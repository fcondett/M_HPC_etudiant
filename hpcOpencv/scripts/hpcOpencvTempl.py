#!/usr/bin/env python3

import matplotlib as mpl
mpl.use('TkAgg')
from matplotlib import pyplot as plt
import cv2 as cv
import time
import sys
import numpy as np



if __name__ == '__main__':

    cap = cv.VideoCapture("data/road_traffic.mp4")  # lit depuis un fichier
    temp = cv.imread("data/road_auto.png",0)
    meth = cv.TM_CCOEFF_NORMED
    seuil = 0.7
    w, h = temp.shape[::-1]
    if temp.size == 0:
        print("failed to load", "data/road_auto.png")
        sys.exit(-1)

    while(cap.isOpened()):
        ret, frame = cap.read()
        if not ret:
            break

        frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        res = cv.matchTemplate(frame, temp, meth)
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
        if(max_val >= seuil):
            top_left = max_loc
            bottom_right = (top_left[0] + w, top_left[1] + h)
            cv.rectangle(frame,top_left, bottom_right, 255, 2)
        cv.imshow('road traffic', frame)
        if cv.waitKey(30) == 27:
            break
        print("maxval=" + str(max_val) + ", maxLoc=" + str(max_loc))