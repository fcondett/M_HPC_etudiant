#!/usr/bin/env python3

import cv2 as cv
import time
import sys

if __name__ == '__main__':

    # arguments
    if len(sys.argv) != 3:
        print("usage:", sys.argv[0], "<filename> <coefficient>")
        sys.exit(-1)
    FILENAME = sys.argv[1]

    COEF = float(sys.argv[2])

    # load input image
    imgIn = cv.imread(FILENAME)
    if imgIn.size == 0:
        print("failed to load", FILENAME)
        sys.exit(-1)

    t0 = time.time()
    
    imgOut = imgIn * COEF

    cv.imwrite("out_mult.png", imgOut)

    imgMult = cv.imread("out_mult.png")
    cv.imshow("image kermorvan.png", imgMult)

    while True:
        k = cv.waitKey() & 0xFF
        if (k == 46):
            break

