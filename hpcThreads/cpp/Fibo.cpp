#include <fstream>
#include <iostream>
#include <thread>
#include <vector>
namespace Fibo {

  // calcule le Nieme terme de la suite de "Fibonacci modulo 42"
  // precondition : N >= 0
  int FibonacciMod42(int N) {
    int f_curr = 0;
    int f_prec = 1;
    for (int i=1; i<=N; i++) {
      int tmp = f_curr;
      f_curr = (f_curr + f_prec) % 42;
      f_prec = tmp;
    }
    return f_curr;
  }

  //////////////////////////////////////////////////////////////////////

  // fonction pour repartir les calculs
  void calculerTout(std::vector<int> &data) {
    // effectue tous les calculs
    for (unsigned i=0; i<data.size(); i++) {
      data[i] = FibonacciMod42(i);
    }
  };

  std::vector<int> fiboSequentiel(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calcule les donnees sequentiellement
    calculerTout(data);

    return data;
  }

//////////////////////////////////////////////////////////////////////

  // fonction pour repartir les calculs
  void calculerPartie(std::vector<int> &data, int x0, int x1) {
    // effectue tous les calculs
    for (int i=x0; i<x1; i++) {
      data[i] = FibonacciMod42(i);
    }
  };

  std::vector<int> fiboBlocs(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur deux threads, par bloc
    std::thread t1(calculerPartie, std::ref(data), 0, nbData / 2);
    std::thread t2(calculerPartie, std::ref(data), nbData / 2, nbData);

    t1.join();
    t2.join();

    return data;
  }

  //////////////////////////////////////////////////////////////////////

// fonction pour repartir les calculs
  void calculerUnSurDeux(std::vector<int> &data, int i) {
    // effectue tous les calculs
    for (i; i<data.size(); i++) {
      data[i] = FibonacciMod42(i);
      i++;
    }
  };

  std::vector<int> fiboCyclique2(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur deux threads, cycliquement
    std::thread t1(calculerUnSurDeux, std::ref(data), 0);
    std::thread t2(calculerUnSurDeux, std::ref(data), 1);

    t1.join();
    t2.join();
    return data;
  }

  //////////////////////////////////////////////////////////////////////


// fonction pour repartir les calculs
  void calculerUnSurN(std::vector<int> &data, int i) {
    // effectue tous les calculs
    for (i; i<data.size(); i++) {
      data[i] = FibonacciMod42(i);
      i++;
    }
  };

  std::vector<int> fiboCycliqueN(int nbData, int nbProc) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    std::vector<std::thread> threads;

    for(int i = 0; i < nbProc; i++)
    {
      threads.push_back(std::thread(calculerUnSurN, std::ref(data), i));
    }

    for(int i = 0; i < nbProc; i++)
    {
      threads[i].join();
    }

    return data;
  }

  //////////////////////////////////////////////////////////////////////

  void fiboCycliqueNFake(int nbData, int nbProc) {
    // calculer sur N threads, cycliquement, en ignorant le résultat
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    std::vector<std::thread> threads;

    for(int i = 0; i < nbProc; i++)
    {
      threads.push_back(std::thread(calculerUnSurN, std::ref(data), i));
    }

    for(int i = 0; i < nbProc; i++)
    {
      threads[i].join();
    }
  }

}  // namespace Fibo

