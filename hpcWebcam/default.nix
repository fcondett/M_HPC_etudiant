
let

  config = {
    packageOverrides = pkgs: {

      opencv3 = pkgs.opencv3.override (import ../opencv3-opts.nix);

      haskellPackages = pkgs.haskellPackages.override {
        overrides = haskellPackagesNew: haskellPackagesOld: rec {
          opencv = pkgs.haskell.lib.dontCheck haskellPackagesOld.opencv;
        };
      };

    };
  };

  #rev = "18.03";
  #channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  channel = <nixpkgs>;
  pkgs = import channel { config = config; };

  drv = pkgs.haskellPackages.callCabal2nix "webcam" ./. {};

in

if pkgs.lib.inNixShell then drv.env else drv


